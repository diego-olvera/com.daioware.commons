package com.daioware.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class TestCurrency {

	@Test
	public void test() {
		Currency search;
		for(Currency c:Currency.values()) {
			search=Currency.getByAbbreviation(c.getAbbreviation());
			assertNotNull(search);
			assertEquals(c, search);
			
			search=Currency.getBySign(c.getSign());
			assertNotNull(search);
			assertEquals(c, search);
		}
	}
}
