package com.daioware.commons;

import org.junit.Test;

import com.daioware.commons.wrapper.Wrapper;
import com.daioware.commons.wrapper.WrapperString;

import junit.framework.TestCase;

public class TestWrapper extends TestCase{

	@Test
	public void test() {
		Wrapper<String> wrapperString1=new Wrapper<>("1");
		WrapperString wrapperString2=new WrapperString("1");
		Wrapper<String> wrapperString3=new Wrapper<>("1");
		assertEquals(wrapperString1, wrapperString2);
		assertEquals(wrapperString2, wrapperString3);
		assertEquals(wrapperString1, wrapperString3);
	}
}
