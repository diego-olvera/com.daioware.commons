package com.daioware.commons;

import static org.junit.Assert.*;

import org.junit.Test;

import com.daioware.commons.string.StringUtil;

public class TestRandom {

	@Test
	public void test() {
		final int FIRST_TEST_LENGTH=10;
		for(int i=0;i<10;i++) {
			String s=StringUtil.getRandomString(FIRST_TEST_LENGTH,StringUtil.DIGITS_AND_LETTERS);
			//assertTrue(StringUtil.hasDigitsOrLetters(s));
			assertEquals(FIRST_TEST_LENGTH,s.length());
			System.out.println(s);
		}
	}
}
