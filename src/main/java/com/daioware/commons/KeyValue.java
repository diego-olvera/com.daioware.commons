package com.daioware.commons;

public class KeyValue<Key,Value>{
	private Key key;
	private Value value;
	
	public KeyValue(Key key, Value value) {
		setKey(key);
		setValue(value);
	}
	public Key getKey() {
		return key;
	}
	public void setKey(Key key) {
		this.key = key;
	}
	public Value getValue() {
		return value;
	}
	public void setValue(Value value) {
		this.value = value;
	}
	
	
}
