package com.daioware.commons;

import com.daioware.commons.Currency;

public abstract class PriceItem{
	
	protected double value;
	protected Currency currency;
	
	public PriceItem() {
	}
	public PriceItem(double value, Currency currency) {
		setValue(value);
		setCurrency(currency);
	}
	public double getValue() {
		return value;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}	
}
