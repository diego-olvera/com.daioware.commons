package com.daioware.commons;

public class InvalidNewAttributeException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidNewAttributeException(String name) {
		super(name);
	}
}
