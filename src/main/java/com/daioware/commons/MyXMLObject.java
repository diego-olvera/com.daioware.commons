package com.daioware.commons;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public interface MyXMLObject {
	public default String toXMLString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return toString();
		}
	}
	
	public static<T extends MyXMLObject> String toXMLtring(List<T> elements) {
		StringBuilder info=new StringBuilder();
		String separator="";
		info.append("[");
		for(T e:elements) {
			info.append(separator)
				.append(e.toXMLString());
			separator=",";
		}
		info.append("]");
		return info.toString();
	}
}
