package com.daioware.commons.wrapper;

public class WrapperShort extends Wrapper<Short> {

	public WrapperShort() {
		super();
	}

	public WrapperShort(Short value) {
		super(value);
	}
	
}
