package com.daioware.commons.wrapper;

public class WrapperBoolean extends Wrapper<Boolean> {

	public WrapperBoolean() {
		super();
	}

	public WrapperBoolean(Boolean value) {
		super(value);
	}
	
}
