package com.daioware.commons.wrapper;

public class WrapperByte extends Wrapper<Byte> {

	public WrapperByte() {
		super();
	}

	public WrapperByte(Byte value) {
		super(value);
	}
}
