package com.daioware.commons.wrapper;

import java.math.BigDecimal;

public class WrapperBigDecimal extends Wrapper<BigDecimal> {

	public WrapperBigDecimal() {
		super();
	}

	public WrapperBigDecimal(BigDecimal value) {
		super(value);
	}
	
	
	
}
