package com.daioware.commons.wrapper;

import java.math.BigInteger;

public class WrapperBigInteger extends Wrapper<BigInteger>{

	public WrapperBigInteger() {
		super();
	}

	public WrapperBigInteger(BigInteger value) {
		super(value);
	}
	
}
