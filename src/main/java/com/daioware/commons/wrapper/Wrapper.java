package com.daioware.commons.wrapper;

public class Wrapper<T> {
	public T value;
	
	public Wrapper() {		
	}
	public Wrapper(T value) {
		this.value = value;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Wrapper))
			return false;
		try{
			Wrapper<T> other = (Wrapper<T>) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
		}catch(ClassCastException e) {
			return false;
		}
		
		return true;
	}
	
	public String toString() {
		return value!=null?value.toString():"";
	}
}