package com.daioware.commons.wrapper;

public class WrapperLong extends Wrapper<Long>{

	public WrapperLong() {
		super();
	}

	public WrapperLong(Long value) {
		super(value);
	}
	
}
