package com.daioware.commons.wrapper;

public class WrapperFloat extends Wrapper<Float>{

	public WrapperFloat() {
		super();
	}

	public WrapperFloat(Float value) {
		super(value);
	}
		
}
