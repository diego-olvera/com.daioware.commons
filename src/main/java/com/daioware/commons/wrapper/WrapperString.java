package com.daioware.commons.wrapper;


public class WrapperString extends Wrapper<String>{

	public WrapperString() {
		super();
	}

	public WrapperString(String value) {
		super(value);
	}
	
}
