package com.daioware.commons.wrapper;

public class WrapperDouble extends Wrapper<Double> {

	public WrapperDouble() {
		super();
	}

	public WrapperDouble(Double value) {
		super(value);
	}
	
}
