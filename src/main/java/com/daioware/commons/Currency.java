package com.daioware.commons;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Currency{
	DOLLARS("United States Dollar","$$","USD"),
	MEXICAN_PESOS("Mexican peso","$","MXN"),
	EUROS("Euro","\u20AC","EUR"),
	YEN("Yen","\u00A5","JPY")
	;
	private String name;
	private String abbreviation;
	private String sign;
	
	private Currency(String name,String sign,String abbreviation) {
		this.name=name;
		this.sign=sign;
		this.abbreviation=abbreviation;
	}
	public String toString() {
		return name;
	}
	public String getName() {
		return name;
	}
	
	public String getAbbreviation() {
		return abbreviation;
	}
	@JsonValue
	public String getSign() {
		return sign;
	}
	public static Currency getByAbbreviation(String abbreviation) {
		return getByAbbreviationOr(abbreviation,null);
	}
	public static Currency getByAbbreviationOr(String abbreviation,Currency defaultCurrency) {
		for(Currency currency:Currency.values()) {
			if(currency.getAbbreviation().equals(abbreviation)) {
				return currency;
			}
		}
		return defaultCurrency;
	}
	public static Currency getBySign(String sign) {
		return getBySignOr(sign,null);
	}
	public static Currency getBySignOr(String sign,Currency defaultCurrency) {
		for(Currency currency:Currency.values()) {
			if(currency.getSign().equals(sign)) {
				return currency;
			}
		}
		return defaultCurrency;
	}
}
