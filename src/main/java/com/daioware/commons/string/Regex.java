package com.daioware.commons.string;

import java.util.regex.Pattern;

public class Regex {
	private String regex;
	
	private Pattern pattern;
	
	public Regex(String regex) {
		setRegex(regex);
		
	}
	public boolean matches(String s) {
		return pattern.matcher(s).matches();
	}
	public Pattern getPattern() {
		return pattern;
	}
	public String getRegex() {
		return regex;
	}
	public void setRegex(String regex) {
		this.regex = regex;
		pattern=Pattern.compile(regex);
	}
	@Override
	public String toString() {
		return getRegex();
	}
	
}
