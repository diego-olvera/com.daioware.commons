package com.daioware.commons.string;

public class StringPattern {
	
	private String string;
	private double patternCode;

	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
		if(string!=null) {
			setPatternCode(SearchPatternUtil.patternCode(string));
		}
		else {
			setPatternCode(0);
		}
	}

	public double getPatternCode() {
		return patternCode;
	}

	protected void setPatternCode(double patternCode) {
		this.patternCode = patternCode;
	}
	
}
