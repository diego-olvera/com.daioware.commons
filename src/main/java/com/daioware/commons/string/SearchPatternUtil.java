package com.daioware.commons.string;

import com.daioware.math.MathUtil;

public class SearchPatternUtil {

	public static double patternCode(String string,int beg,int end) {
		double hashCode=0;
		for(int i=beg;i<end;i++) {
			hashCode+=string.charAt(i);
		}
		return hashCode;
	}
	public static double patternCode(String string) {
		return patternCode(string,0,string.length());
	}
	
	public static double getSimilarity(String x,String y) {
		/*int sizeX=x.length(),sizeY=y.length();
		int minSize=Math.min(sizeX,sizeY);
		int maxSize=Math.max(sizeX, sizeY);
		*/
		/*
		double patternCodeX=patternCode(x,0,minSize);
		double patternCodeX=patternCode(x,0,minSize);
		 */
		double patternCodeX=patternCode(x);
		double patternCodeY=patternCode(y);

		double sim=MathUtil.crossMultiplication(patternCodeX, 100, patternCodeY);
		/*if(maxSize!=minSize) {
			sim= sim-(100-MathUtil.crossMultiplication(maxSize,100,minSize));
		}*/
		return sim;
	}
	public static void main(String[] args) {
		String s1="hello,ZZZZZZ",s2="hello",s3="olleh",s4="ello";
		System.out.println(patternCode(s1));
		System.out.println(patternCode(s2));
		System.out.println(patternCode(s3));
		System.out.println(patternCode(s4));
		System.out.println("sim:"+getSimilarity(s1,s2));
		System.out.println("sim:"+getSimilarity(s2,s2));

		System.out.println("sim:"+getSimilarity(s2,s3));
		System.out.println("sim:"+getSimilarity(s2,s4));

	}
}
