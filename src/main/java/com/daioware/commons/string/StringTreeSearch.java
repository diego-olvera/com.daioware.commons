package com.daioware.commons.string;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

class Node implements Iterable<Node>{
	public static int DEFAULT_CHARS_SIZE=27;
	
	public Character character;
	public boolean endOfWord;
	private Map<Character,Node> childNodes;
	public Node father;
	
	public Node(int initialChildNodesSize) {
		childNodes=new java.util.HashMap<>();
	}
	public Node() {
		this(DEFAULT_CHARS_SIZE);
	}
	public Node(Node father) {
		this(DEFAULT_CHARS_SIZE);
		this.father=father;
	}
	public Node getChildNodeOrNewOne(Node father,Character c) {
		Node child=childNodes.get(c);
		if(child==null) {
			childNodes.put(c,child=new Node(father));
			child.character=c;
		}
		return child;
	}
	public Node getChildNode(Character c) {
		return childNodes.get(c);
	}
	public Iterator<Node> childNodes(){
		return childNodes.values().iterator();
	}
	@Override
	public Iterator<Node> iterator() {
		return childNodes();
	}
	protected void getWords(List<Character> characters,Consumer<String> consumer){
		StringBuilder endOfWordBuilder;
		int charactersSize;
		if(character!=null) {
			characters.add(character);
		}
		if(endOfWord) {
			endOfWordBuilder=new StringBuilder();
			for(Character c:characters) {
				endOfWordBuilder.append(c);
			}
			consumer.accept(endOfWordBuilder.toString());
		}
		for(Node node:this) {
			node.getWords(characters,consumer);
		}
		charactersSize=characters.size();
		if(charactersSize>=1) {
			characters.remove(charactersSize-1);
		}
	}
	public List<String> getDescendantWords(){
		List<String> words=new LinkedList<>();
		List<Character> characters=new ArrayList<>();
		getWords(characters,(s)->words.add(0,s));
		return words;
	}
	public List<String> getAscendantWords(){
		List<String> words=new ArrayList<>();
		List<Character> characters=new ArrayList<>();
		getWords(characters,(s)->words.add(s));
		return words;
	}
}

public class StringTreeSearch {

	private Node root;
	
	public StringTreeSearch() {
		root=new Node();
	}
	
	public List<String> getWords(){
		return root.getAscendantWords();
	}
	public void insert(String string) {
		Node currentNode=root;
		for(int i=0,j=string.length();i<j;i++) {
			currentNode=currentNode.getChildNodeOrNewOne(currentNode,string.charAt(i));
		}
		currentNode.endOfWord=true;
	}
	public void remove(String string) {
		Node currentNode=root;
		for(int i=0,j=string.length();currentNode!=null && i<j;i++) {
			currentNode=currentNode.getChildNode(string.charAt(i));
		}
		if(currentNode!=null && currentNode.endOfWord) {
			currentNode.endOfWord=false;
		}
	}
	public boolean existsExactWord(String string) {
		Node currentNode=root;
		for(int i=0,j=string.length();currentNode!=null && i<j;i++) {
			currentNode=currentNode.getChildNode(string.charAt(i));
		}
		return currentNode!=null?currentNode.endOfWord:false;
	}
	public List<String> findSimilarWords(String string) {
		Node currentNode=root;
		List<String> similarWords=new ArrayList<>();
		for(int i=0,j=string.length();currentNode!=null && i<j;i++) {
			currentNode=currentNode.getChildNode(string.charAt(i));
			if(currentNode==null) {
				
			}
		}
		return similarWords;
	}
	public static void main(String[] args) {
		String word="Diego";
		StringTreeSearch tree=new StringTreeSearch();
		tree.insert(word);
		tree.insert(word +" Olvera");
		tree.insert("Incredible");
		tree.insert("Diego Incredible");

		System.out.println(tree.existsExactWord(word));
		
		tree.remove(word);

		for(String s:tree.getWords()) {
			System.out.println("s:"+s);
		}
	}
}
