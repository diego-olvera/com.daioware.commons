package com.daioware.commons;

import java.util.List;

public class JsonPrimitives<Type> extends JsonFormatter {
	
	public JsonPrimitives() {
		super();
	}
	public void add(List<Type> list) {
		list.forEach((v)->add(v));
	}
	public void add(Type[] list) {
		for(Type t:list) {
			add(t);
		}
	}
	public JsonPrimitives<Type> add(Type value) {
		add(null,value);
		return this;
	}
	public String toString() {
		StringBuilder info=new StringBuilder();
		String sep="";
		info.append("[");
		for(KeyValue<String, Object> obj:this) {
			info.append(sep);
			info.append(getValue(obj.getValue()));
			sep=",";
		}
		info.append("]");
		return info.toString();
	}
}
