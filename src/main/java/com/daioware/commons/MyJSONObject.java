package com.daioware.commons;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public interface MyJSONObject {
	public default String toJSONString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return toString();
		}
	}
	
	public static<T extends MyJSONObject> String toJSONString(List<T> elements) {
		StringBuilder info=new StringBuilder();
		String separator="";
		info.append("[");
		for(T e:elements) {
			info.append(separator)
				.append(e.toJSONString());
			separator=",";
		}
		info.append("]");
		return info.toString();
	}
}
