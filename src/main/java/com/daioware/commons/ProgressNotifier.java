package com.daioware.commons;

public class ProgressNotifier {
	
	private float currentProgress;
	private Float totalProgress;
	
	public ProgressNotifier(float currentProgress, Float totalProgress) {
		setCurrentProgress(0);
		setTotalProgress(totalProgress);
	}
	
	public ProgressNotifier(Float totalProgress) {
		this(0,totalProgress);
	}

	public Float getCurrentProgress() {
		return currentProgress;
	}
	public Float getTotalProgress() {
		return totalProgress;
	}
	public void setCurrentProgress(float currentProgress) {
		this.currentProgress = currentProgress;
	}
	public void setTotalProgress(Float totalProgress) {
		this.totalProgress = totalProgress;
	}
	
	public Float getPercentageCompleted() {
		Float totalProgress=getTotalProgress();
		Float currentProgress=getCurrentProgress();
		return totalProgress!=null && currentProgress!=null?currentProgress*100/(float)totalProgress:null;
	}
	
}
