package com.daioware.commons;

public class Util implements UnicodeCharacters { 
	 
	public static final char A_UPPER_CASE=65;
	public static final char Z_UPPER_CASE=90;
	public static final String JUMP_LINE="\r\n";
	public static final char ZERO=48;
	public static final char NINE=57; 
	
	
	public static final int JUMP_LINES=25;
	public static final int CHARACTERS_PER_ROW=80;
	static final int NUM_VALID_OF_SEPARATORS=2;
	
	public static void cleanScreen(int saltosDeLinea){
		for(int i=0;i<saltosDeLinea;i++)
			System.out.println();
	}
	public static void cleanScreen(){
		cleanScreen(JUMP_LINES);
	}
	public static void pause(long milisegundos){
		try{
			Thread.sleep(milisegundos);
		}catch(InterruptedException e){
			e.printStackTrace();
		}
	}
	public static void sleep(long milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static void pause(String mensaje){
		System.out.print(mensaje);
		ScanF.readChar(); 
	}
	public static void pause(){
		pause("\nPress enter to continue . . .");
	}
	public static void printCentered(String texto,String relleno){
	    System.out.println(getCenteredString(texto, relleno)); 
	}
	public static String getCenteredString(String texto, String relleno){
		StringBuilder cadenaCentrada= new StringBuilder();
		int longitudTexto=texto.length();
	    int espacios=(CHARACTERS_PER_ROW-longitudTexto)/2;
	    int i;
	    for(i=0;i<espacios;i++){
	        cadenaCentrada.append(relleno);
	    }
	    cadenaCentrada.append(texto);   
	    for(i+=longitudTexto;i<CHARACTERS_PER_ROW;i++){
	    	cadenaCentrada.append(relleno);
	    }
	    return cadenaCentrada.append("\n").toString();
	}	
}
