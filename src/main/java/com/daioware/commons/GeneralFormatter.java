package com.daioware.commons;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class GeneralFormatter implements Iterable<KeyValue<String,Object>>{
	
	private List<KeyValue<String,Object>> fields;
	
	private String name;
	
	public GeneralFormatter() {
		this(null);
	}
	public GeneralFormatter(String name) {
		setName(name);
		fields=new LinkedList<>();
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public GeneralFormatter add(String name,Object value) {
		fields.add(new KeyValue<String,Object>(name, value));
		return this;
	}
	public Object remove(String name) {
		Object removed=null;
		for(KeyValue<String, Object> obj:this) {
			if(obj.getKey().equals(name)) {
				removed=obj;
				break;
			}
		}
		if(removed!=null) {
			fields.remove(removed);
			return removed;
		}
		else {
			return null;
		}		
	}
	public Object get(String name) {
		for(KeyValue<String, Object> obj:this) {
			if(obj.getKey().equals(name)) {
				return obj;
			}
		}	
		return null;
	}

	@Override
	public Iterator<KeyValue<String, Object>> iterator() {
		return fields.iterator();
	}
	
}
