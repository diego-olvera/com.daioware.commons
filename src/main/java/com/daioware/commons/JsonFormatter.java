package com.daioware.commons;

import java.util.ArrayList;
import java.util.List;

public class JsonFormatter extends GeneralFormatter{
	
	
	public JsonFormatter() {
		super();
	}
	public JsonFormatter(String name) {
		super(name);
	}
	public static String getValue(Object value) {
		StringBuilder info=new StringBuilder();
		if(value instanceof String) {
			info.append("\"").append(value).append("\"");
		}
		else if(value instanceof Boolean) {
			info.append(((Boolean)value).booleanValue()?"true":"false");
		}
		else {
			info.append(value);
		}
		return info.toString();
	}
	public String toString() {
		StringBuilder info=new StringBuilder();
		Object key;
		String name=getName();
		boolean nameNotNull=name!=null;
		String sep="";
		if(nameNotNull) {
			info.append("{\"").append(name).append("\":");
		}
		info.append("{");
		for(KeyValue<String, Object> obj:this) {
			info.append(sep);
			key=obj.getKey();
			if(key!=null) {
				info.append("\"").append(key).append("\":");
			}
			info.append(getValue(obj.getValue()));
			sep=",";
		}
		info.append("}");
		if(nameNotNull) {
			info.append("}");
		}
		return info.toString();
	}
	public static void main(String[] args) {
		JsonFormatter json=new JsonFormatter("myElement");
		/*json.add("integer",10)
			.add("string","hello")
			.add("boolean",false)
			.add("null attribute",null)
			.add("otherElement",
					new JsonFormatter()
						.add("value1",1)
						.add("value2",10)
			)		
		;*/
		String strings[]= {"hi","ho","hi"};
		Integer integers[]= {1,2,3};
		List<GeneralFormatter> listStrings=new ArrayList<>();
		JsonPrimitives<Integer> primitive=new JsonPrimitives<>();
		json.add("strings",listStrings);
		json.add("integers",primitive);
		for(String s:strings) {
			listStrings.add(new JsonFormatter().add("text",s));
		}
		primitive.add(integers);
		System.out.println(json);
			
	}
}
